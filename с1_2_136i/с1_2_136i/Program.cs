﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace с1_2_136i
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b=0, c=1, fact=1;
            Console.WriteLine("Введите число слогаемых");
            double n = Convert.ToDouble(Console.ReadLine());
            for (int i=1; i<=(n);i++)                                       //цикл сумирования слагаемых an/(n-1)!
            {
                Console.WriteLine("Введите число");
                a = Convert.ToDouble(Console.ReadLine());
                for (int e=0; e<i; e++)                                     //высчитываем факториал
                {
                    fact = fact * (e);
                    if (fact <= 0) { fact = 1; }
                }                                                           
                a = a / fact;                                               //находим n-ое слагаемое
                b = b + a;                                                  //складываем n-ое слагаемое со слагаемым n-1
            }
            Console.WriteLine("Ответ=" + b);
            Console.Read();
        }
    }
}
