﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharpc2
{
    class Program
    {
        
        static void Main(string[] args)
        {
            double a, l, n,  x = 0, y=0;            
            Console.WriteLine("Введите количесво отрезков");
            n = Convert.ToDouble(Console.ReadLine());
            for (int i = 1; i <= n; i++) 
            {
                Console.WriteLine("Введите угол");
                a = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите длину");
                l = Convert.ToDouble(Console.ReadLine());
                a = (a * Math.PI)/180;              //Перевод градусов в радианы
                x = x + l * Math.Cos(a);            //Вычисление координаты x 
                y = y + l * Math.Sin(a);            //Вычисление координаты y
            }
            Console.WriteLine("Координаты по x=" + x);
            Console.WriteLine("Координаты по y=" + y);
            Console.ReadLine();
        }
    }
}
