﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C6_1_465
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку");
            string str = Console.ReadLine();
            Console.WriteLine("Введите искомый символ");
            char sim =Convert.ToChar(Console.ReadLine());
            //построение процедуры по опр позиции самого первого вхождения заданного символа в исходную строку
            int ind = -1;
            for (int i = str.Length-1; i >= 0; i--)
            {
                if (str[i].Equals(sim))
                {
                    ind = i;
                    break;
                }
            }
            Console.WriteLine(ind);
            Console.Read();
        }
    }
}
