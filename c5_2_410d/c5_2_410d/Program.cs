﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c5_2_410d
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину матрицы=");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите ширину матрицы=");
            int m = Convert.ToInt32(Console.ReadLine());
            Random rnd = new Random();
            
            int[,] mass = new int[n, m];
            for (int i=0; i<n; i++)
            {
                for (int j=0; j<m; j++)
                {
                    int r = rnd.Next(10, 49);
                    mass[i, j] = r;
                    Console.Write(mass[i,j]+" ");
                }

                Console.WriteLine();
                
            }
            Console.WriteLine();
            Console.WriteLine("Транспонированая матрица:");

            int[,] masst = new int [ m, n];

            for (int j = 0; j < m; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    masst[j, i] = mass[i, j];
                    Console.Write(masst[j, i] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("Прямое произведение транспонированной матрицы:");
            int[,] massp = new int[m, n];

            for (int j = 0; j < m; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    massp[j, i] = masst[j, 0] + masst[0, i];
                    massp[j, 0] = masst[j, 0];
                    massp[0, i] = masst[0, i];
                    
                    Console.Write(massp[j, i] + " ");
                }
                Console.WriteLine();
            }
            
                    Console.Read();
        }
    }
}
