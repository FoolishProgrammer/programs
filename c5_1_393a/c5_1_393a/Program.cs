﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c5_1_393a
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите размер квадратной матрицы=");
            int n = Convert.ToInt32(Console.ReadLine());

            int i, j;
            Random r = new Random();
            int min = 0;
            int m = n;
            int[,] mass = new int [n,m];
            for ( i=0; i<n; i++)
            {
                for ( j =0; j<m; j++)
                {
                    mass[i, j] = r.Next(10,99);
                    Console.Write(mass[i, j]+" ");
                }
                Console.WriteLine();

            }
            Console.Write("Введите номер строки=");
            int s = Convert.ToInt32(Console.ReadLine());
            
            min = mass[0, 0];
            for(i=0; i<s; i++)
            {
                for(j=0; j<s; j++)
                {
                    i = s-1;
                    if (min>=mass[i,j])
                    {
                        min = mass[i, j];
                    }
                }
            }
            Console.Write("Минимальное значение на основной диагонли выбраной строки " + s + "= " + min);            
                        Console.Read();
        }
    }
}
