﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c4_1_336a
{
    class Program
    {
        static void Main(string[] args)
        {
            double fact2 = 1, fact1 = 1,d=0;
            Console.WriteLine("Введите верхнюю границу суммирования n");
            double n = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите число x");
            double x = Convert.ToDouble(Console.ReadLine());
            for (int i = 1; i <= n; i++) 
            {
                
                    for (int a = 1; a <= (2 * i); a++)
                    {
                        fact1 = fact1 * a;                    //Вычисление (2i)!

                    }
                    for (int a = 1; a <= (Math.Pow(i, 2)); a++)
                    {
                        fact2 = fact2 * a;                    //Вычисление  (i^2)!

                    }
                    d = d + ((fact1 + Math.Abs(x)) / fact2);
                
            }
            Console.WriteLine("Ответ: " + d);
            Console.Read();
        }
    }
}
